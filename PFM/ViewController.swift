//
//  ViewController.swift
//  PFM
//
//  Created by Дмитрий Кашин on 10/06/2019.
//  Copyright © 2019 Дмитрий Кашин. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    
    private var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView = WKWebView(frame: .zero)
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        webView.backgroundColor = .red
        view.backgroundColor = .gray
        
        if let path = Bundle.main.url(forResource: "cashoff", withExtension: "html"),
            let text = try? String(contentsOf: path, encoding: String.Encoding.utf8) {
            webView.loadHTMLString(text, baseURL: path)
        }
    }
}
